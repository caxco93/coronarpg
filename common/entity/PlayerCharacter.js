import nengi from "nengi";
import WeaponSystem from "../WeaponSystem";
import SAT from "sat";

class PlayerCharacter {
  constructor(infected) {
    this.x = 0;
    this.y = 0;
    this.rotation = 0;
    this.hitpoints = 100;
    this.isAlive = true;
    this.infected = infected;

    this.moveDirection = {
      x: 0,
      y: 0,
    };

    this.speed = 300;
    this.currentSpeed = this.speed;
    this.sanitizerMultiplier = 1.5;

    this.boostDuration = 5;
    this.boostTimer = 0;

    this.weaponSystem = new WeaponSystem();

    this.collider = new SAT.Circle(new SAT.Vector(this.x, this.y), 25);
  }

  takeDamage(amount) {
    if (this.isAlive) {
      this.hitpoints -= amount;
    }

    if (this.hitpoints <= 0 && this.isAlive) {
      this.hitpoints = 0;
      this.isAlive = false;

      // DEAD! come back to life and teleport to a new spot
      setTimeout(() => {
        this.hitpoints = 100;
        this.isAlive = 100;
        this.x = Math.random() * 500;
        this.y = Math.random() * 500;
      }, 1000);
    }
  }

  fire() {
    if (!this.isAlive) {
      return false;
    }

    return this.weaponSystem.fire();
  }

  infect() {
    this.infected = true;
  }

  touchSanitizer() {
    this.boostTimer = 0;
    this.boosted = true;
    this.currentSpeed = this.speed * this.sanitizerMultiplier;
  }

  processMove(command) {
    if (!this.isAlive) {
      return;
    }

    this.rotation = command.rotation;

    let unitX = 0;
    let unitY = 0;

    // create forces from input
    if (command.forward) {
      unitY -= 1;
    }
    if (command.backward) {
      unitY += 1;
    }
    if (command.left) {
      unitX -= 1;
    }
    if (command.right) {
      unitX += 1;
    }

    // normalize
    const len = Math.sqrt(unitX * unitX + unitY * unitY);
    if (len > 0) {
      unitX = unitX / len;
      unitY = unitY / len;
    }

    this.moveDirection.x = unitX;
    this.moveDirection.y = unitY;
  }

  move(delta) {
    const infectedSpeedFactor = 0.5;
    const dx =
      this.moveDirection.x *
      this.currentSpeed *
      delta *
      (this.infected ? infectedSpeedFactor : 1);
    const dy =
      this.moveDirection.y *
      this.currentSpeed *
      delta *
      (this.infected ? infectedSpeedFactor : 1);
    this.x += dx;
    this.y += dy;

    if (this.x < 0) {
      this.x = 0;
    }
    if (this.x > 2000) {
      this.x = 2000;
    }
    if (this.y < 0) {
      this.y = 0;
    }
    if (this.y > 2000) {
      this.y = 2000;
    }

    this.collider.pos.x = this.x;
    this.collider.pos.y = this.y;

    if (this.boosted == true) {
      this.boostTimer += delta;
      if (this.boostTimer > this.boostDuration) {
        this.currentSpeed = this.speed;
        this.boosted = false;
        this.boostTimer = 0;
      }
    }
  }
}

PlayerCharacter.protocol = {
  x: { type: nengi.Float32, interp: true },
  y: { type: nengi.Float32, interp: true },
  rotation: { type: nengi.RotationFloat32, interp: true },
  isAlive: nengi.Boolean,
  hitpoints: nengi.UInt8,
  infected: nengi.Boolean,
};

export default PlayerCharacter;
