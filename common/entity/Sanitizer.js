import nengi from "nengi";
import SAT from "sat";

class Sanitizer {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.collider = new SAT.Circle(new SAT.Vector(this.x, this.y), 25);
  }
}

Sanitizer.protocol = {
  x: { type: nengi.Float32, interp: false },
  y: { type: nengi.Float32, interp: false },
};

export default Sanitizer;
