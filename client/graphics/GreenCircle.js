import * as PIXI from "pixi.js";
import HitpointBarEnemy from "./HitpointBarEnemy";

class GreenCircle extends PIXI.Container {
  constructor(entity) {
    super();
    this.x = entity.x;
    this.y = entity.y;
    this.isAlive = entity.isAlive; // not really used...

    // this.hitpointBar = new HitpointBarEnemy();
    // this.hitpointBar.x = -6;
    // this.hitpointBar.y = -20;
    // this.hitpointBar.setHitpointPercentage(entity._hitpoints / 100);

    this._hitpoints = 0;
    this.hitpoints = entity.hitpoints;

    this.body = new PIXI.Graphics();
    this.body.beginFill(0xffffff);
    this.body.drawStar(0, 0, 800, 40);
    this.body.endFill();

    this.body.tint = 0x75c9b1;

    this.addChild(this.body);

    // this.addChild(this.hitpointBar);
  }

  set hitpoints(value) {
    // this._hitpoints = value;
    // this.hitpointBar.setHitpointPercentage(value / 100);
  }

  update(delta) {}
}

export default GreenCircle;
