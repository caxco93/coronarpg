import * as PIXI from "pixi.js";

class HitpointBarEnemy extends PIXI.Container {
  constructor() {
    super();

    this.background = new PIXI.Graphics();
    this.background.beginFill(0xbc3232);
    this.background.drawRect(0, 0, 8, 40);
    this.background.endFill();
    this.addChild(this.background);

    this.foreground = new PIXI.Graphics();
    this.foreground.beginFill(0xff6e4a);
    this.foreground.drawRect(1, 1, 6, 38);
    this.foreground.endFill();
    this.addChild(this.foreground);
  }

  setHitpointPercentage(percent) {
    this.foreground.scale.y = percent;
  }
}

export default HitpointBarEnemy;
