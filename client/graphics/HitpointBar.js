import * as PIXI from "pixi.js";
class HitpointBar extends PIXI.Container {
  constructor() {
    super();

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x579583);
    this.background.drawRect(5, 0, 8, 40);
    this.background.endFill();
    this.addChild(this.background);

    this.foreground = new PIXI.Graphics();
    this.foreground.beginFill(0x72c0a9);
    this.foreground.drawRect(6, 1, 6, 38);
    this.foreground.endFill();
    this.addChild(this.foreground);
  }

  setHitpointPercentage(percent) {
    this.foreground.scale.y = percent;
  }
}

export default HitpointBar;
