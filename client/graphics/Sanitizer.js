import * as PIXI from "pixi.js";
import HitpointBar from "./HitpointBar";

class GreenCircle extends PIXI.Container {
  constructor(entity) {
    super();

    const texture = PIXI.Texture.from("images/mask.png");
    const sprite = new PIXI.Sprite(texture);
    sprite.x = entity.x;
    sprite.y = entity.y;
    sprite.width = 64;
    sprite.height = 48;
    sprite.pivot.x = 32;
    sprite.pivot.y = 32;

    this.addChild(sprite);
  }

  set hitpoints(value) {
    this._hitpoints = value;
    this.hitpointBar.setHitpointPercentage(value / 100);
  }

  update(delta) {}
}

export default GreenCircle;
